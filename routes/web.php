<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('index');

//Ruta para login
Route::post('/login', [
    'as' => 'login', 
    'uses' => 'Auth\LoginController@login'
]);

//Ruta para el dashboard
Route::get('/dashboard', 'DashboardController@index')->name('dash.historial');

//Ruta para votar positivo 
Route::get('/favor/{id}', 'DashboardController@favor')->name('voto.favor');

//Ruta para votar negativo  
Route::get('/contra/{id}', 'DashboardController@contra')->name('voto.contra');

//Ruta para votar nulo 
Route::get('/nulo/{id}', 'DashboardController@nulo')->name('voto.nulo');