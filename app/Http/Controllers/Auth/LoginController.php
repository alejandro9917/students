<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $fields = $this->validate($request,[
            'cod_carnet' => 'required|min:7'
        ]);

        $credentials = $request->only('cod_carnet', 'password');

        if(Auth::attempt($credentials, true))
        {
            return redirect()->route('dash.historial');
        }

        return 'No se puede';
    }
}
