<?php

namespace App\Http\Controllers;

use App\Theme;
use App\Vote;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $temas = Theme::get();  
        return view('historial')->with('temas', $temas);
    }

    public function favor($id)
    {
        //Se valida el estudiante ya voto por ese tema
        $votos = Vote::where([
            ['id_carnet', Auth::User()->id],
            ['id_tema', $id]
            ])->get();

        if(count($votos))
        {
            return redirect()->route('dash.historial');
        }

        else
        {
            //Se crea el voto a favor, valor 1 es para votar a favor
            Vote::create([
                'id_carnet' => Auth::User()->id,
                'id_tema' => $id,
                'num_valor' => 1,
            ]);

            return redirect()->route('dash.historial');
        }  
    }

    public function contra($id)
    {
        //Se valida el estudiante ya voto por ese tema
        $votos = Vote::where([
            ['id_carnet', Auth::User()->id],
            ['id_tema', $id]
            ])->get();

        if(count($votos))
        {
            return redirect()->route('dash.historial');
        }

        else
        {
            //Se crea el voto a favor, valor 2 es para votar en contra
            Vote::create([
                'id_carnet' => Auth::User()->id,
                'id_tema' => $id,
                'num_valor' => 2,
            ]);

            return redirect()->route('dash.historial');
        }
    }

    public function nulo($id)
    {
        //Se valida el estudiante ya voto por ese tema
        $votos = Vote::where([
            ['id_carnet', Auth::User()->id],
            ['id_tema', $id]
            ])->get();

        if(count($votos))
        {
            return redirect()->route('dash.historial');
        }

        else
        {
            //Se crea el voto a favor, valor 0 es para votar nulo
            Vote::create([
                'id_carnet' => Auth::User()->id,
                'id_tema' => $id,
                'num_valor' => 0,
            ]);

            return redirect()->route('dash.historial');
        }
    }
}